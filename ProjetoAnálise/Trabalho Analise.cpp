//Nomes:
//Matheus De Melo Ribeiro  CCP
//Danilo Henrique  CCP
//Rodrigo Conte  CCP

//Analise de algoritmos Noturno

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <inttypes.h>

#define NUMCARACT 10

long int numeroDeElementosDoArquivoDeEntrada = 0;

typedef struct arquivo{
    FILE* f;
    int *buffer;
    long int pos,max;
}ARQ;

void preencherBuffer(ARQ* arq,long int K);
int procuraMenor(ARQ* arq, int numArqs, long int K, long int* menor);
void merge(char URLPasta[],int numArqs,long int K);
void Quick(int vetor[10], int inicio, long int fim);
int dividirArquivoDeEntradaEmVariasPartesOrdenadas(char URLPastaDestino[],double memoriaMaximaEmMB);
void pedirInformacoesDoUsuario(char URLPastaArquivoDeEntrada[], char URLPastaArquivoDeSaida[], double *memoriaMaximaEmMB, int *K);
int verificarExistenciaDeArquivo(char URLArq[]);
int verificarExistenciaDaPasta(char URLArq[]);
int gerarArquivoDeEntrada(char URL[], int maiorValorQuePodeSerGerado, long double tamanhoDoArquivoEmKB);
int numeroDeCaracteresDentroDeUmInteiro(int inteiro);
void removerArquivosDivisoes(char url[], int numArqs, int excluir); // '0' para manter e '1' para excluir
void escreverZerosRestantesNoArquivo(FILE* arq, int valor);

int main(){
    
    
    char URLPastaArquivoDeEntrada[200];
    char URLPastaArquivoDeSaida[200];
    double memoriaMaximaEmMB;
    int K;
    
    pedirInformacoesDoUsuario(URLPastaArquivoDeEntrada, URLPastaArquivoDeSaida, &memoriaMaximaEmMB, &K);
    
    int numArqs = dividirArquivoDeEntradaEmVariasPartesOrdenadas(URLPastaArquivoDeEntrada, memoriaMaximaEmMB);
    
    merge(URLPastaArquivoDeEntrada, numArqs, (memoriaMaximaEmMB*1024*1024/11) / (numArqs+1) );
    
    removerArquivosDivisoes(URLPastaArquivoDeEntrada, numArqs, 0);
    
    printf("\nPrograma finalizado!\n");
    
    return 0;
}

// Preenche os buffers de memória com os valores se seus arquivos correspondentes
void preencherBuffer(ARQ* arq,long int K){
    int i;
    if (arq->f == NULL) {
        return;
    }else{
        arq->pos = 0;
        arq->max = 0;
        for (i=0; i<K; i++) {
            if (!feof(arq->f)) {
                int a;
                fscanf(arq->f, "%d ",&a);
                arq->buffer[arq->max] = a;
                
                arq->max += 1;
            }else{
                fclose(arq->f);
                arq->f = NULL;
                break;
            }
        }
    }
}

// Procura o menor valor dentro do buffer de memoria
int procuraMenor(ARQ* arq, int numArqs, long int K, long int* menor){
    
    int i, idx = -1;
    
    for (i=0; i<numArqs; i++) {
        if (arq[i].pos < arq[i].max) {
            if (idx == -1) {
                idx = i;
            }else{
                if (arq[i].buffer[arq[i].pos] < arq[idx].buffer[arq[idx].pos]) {
                    idx = i;
                }
            }
        }
    }
    
    if (idx != -1) {
        *menor = arq[idx].buffer[arq[idx].pos];
        arq[idx].pos++;
        
        if (arq[idx].pos == arq[idx].max) {
            preencherBuffer(&arq[idx],K);
        }
        return 1;
    } else {
        return 0;
    }
}

// Método que realiza todo o merge sort externo
void merge(char URLPasta[],int numArqs,long int K){
    
    char novo[100];
    int *buffer = (int*)malloc(K*sizeof(int));
    
    ARQ* arq = (ARQ*)malloc(numArqs*sizeof(ARQ));
    
    int i;
    for(i=0;i<numArqs;i++){
        char URLAux[100];
        
        strcpy(URLAux, URLPasta);
        
        sprintf(novo, "/divisao%d.dat",i+1);
        
        strcat(URLAux, novo);
        
        arq[i].f = fopen(URLAux, "r");
        if (arq[i].f == NULL) {
            printf("Arquivo Nulo!");
            exit(0);
        }else{
            arq[i].max = 0;
            arq[i].pos = 0;
            arq[i].buffer = (int*)malloc(K*sizeof(int));
            preencherBuffer(&arq[i],K);
        }
    }
    
    long int menor; 
	int qtdBuffer = 0;
    while (procuraMenor(arq,numArqs,K,&menor) == 1) {
        buffer[qtdBuffer] = menor;
        qtdBuffer++;
        
        if (qtdBuffer == K) {
            
            char URLAux[100];
            strcpy(URLAux, URLPasta);
            char URLAux2[100];
            strcpy(URLAux2, "/Arquivo De Saida.dat");
            strcat(URLAux, URLAux2);
            FILE* f = fopen(URLAux, "a");
            int j;
            for (j=0; j<K-1; j++) {
                escreverZerosRestantesNoArquivo(f, buffer[j]);
                fprintf(f, "%d ",buffer[j]);
            }
            fclose(f);
            qtdBuffer = 0;
        }
    }
    
    if (qtdBuffer != 0) {
        
        char URLAux[100];
        strcpy(URLAux, URLPasta);
        char URLAux2[100];
        strcpy(URLAux2, "/Arquivo De Saida.dat");
        strcat(URLAux, URLAux2);
        FILE* f = fopen(URLAux, "a");
        int j;
        for (j=0; j<qtdBuffer-1; j++) {
            escreverZerosRestantesNoArquivo(f, buffer[j]);
            fprintf(f, "%d ",buffer[j]);
        }
        fclose(f);
    }
    
    for (i=0; i<numArqs; i++) {
        free(arq[i].buffer);
    }
    free(arq);
    free(buffer);
    
}

// Método de ordenação usado para ordenar valores locais
void Quick(int vetor[10], int inicio, long int fim){
    
    long int pivo, aux, i, j, meio;
    
    i = inicio;
    j = fim;
    
    meio = (int) ((i + j) / 2);
    pivo = vetor[meio];
    
    do{
        while (vetor[i] < pivo) i = i + 1;
        while (vetor[j] > pivo) j = j - 1;
        
        if(i <= j){
            aux = vetor[i];
            vetor[i] = vetor[j];
            vetor[j] = aux;
            i = i + 1;
            j = j - 1;
        }
    }while(j > i);
    
    if(inicio < j) Quick(vetor, inicio, j);
    if(i < fim) Quick(vetor, i, fim);
    
}

// Remove arquivos contidos em 'url2[]' que não interessam para o usuário, mas são essenciais para o funcionamento do programa. Recebe por parâmetro o valor de K. Os parâmetros 'excluirDivisoes', 'excluirOrdenados' e 'excluirArquivosAuxiliares' recebem 0 ou 1 (0 para não excluir e 1 para excluir)
void removerArquivosDivisoes(char url2[], int numArqs, int excluir){
    
    int cont;
    for (cont=1; cont<=numArqs; cont++) {
        char url[100];
        char aux[100];
        
        if (excluir) {
            strcpy(url, url2);
            sprintf(aux, "/divisao%d.dat",cont);
            strcat(url, aux);
            
            remove(url);
        }
        
    }
    
}

// Divide o arquivo de entrada em varias partes dependendo da quantidade de memoria máxima que o usuário informou
int dividirArquivoDeEntradaEmVariasPartesOrdenadas(char URLPastaDestino[],double memoriaMaximaEmMB){
    
    char URL[100];
    strcpy(URL, URLPastaDestino);
    strcat(URL, "/Arquivo De Entrada.dat");
    
    FILE *arq = fopen(URL, "r");         // Carrega o arquivo de entrada
    
    if (arq == NULL) {
        printf("\nNao foi possivel ler o arquivo de entrada para gerar os arquivos divisao.dat!");
    } else {
        
        long int acumularTam = 0;
        double memoriaMaximaEmBy = memoriaMaximaEmMB*1024*1024;
        int aux;
        long int tamVet=0;
        long int contVet=0;
        int contArq=0;
        
        int *vet = (int*)malloc((memoriaMaximaEmBy/11)*sizeof(int));
        
        while (fscanf(arq, "%d ",&aux) != EOF) {
            
            vet[contVet] = aux;
            contVet++;
            tamVet++;
            acumularTam += 11;
            
            if (acumularTam + 11 >= memoriaMaximaEmBy) {
                
                char URL2[100];
                char URLAux[100];
                
                strcpy(URL2, URLPastaDestino);
                contArq++;
                sprintf(URLAux, "/divisao%d.dat",contArq);
                strcat(URL2, URLAux);
                
                FILE* arq2 = fopen(URL2, "w");
                
                if (arq2 == NULL) {  // Isso nao pode acontecer
                    printf("Arquivo divisao%d.dat não pode ser criado!",contArq);
                    exit(0);
                } else {
                    
                    Quick(vet, 0, tamVet-1);
                    
                    long int count;
                    for (count=0; count<tamVet; count++) {
                        
                        escreverZerosRestantesNoArquivo(arq2, vet[count]);
                        
                        fprintf(arq2, "%d ",vet[count]);
                    }
                    fclose(arq2);
                }
                
                acumularTam = 0;
                contVet=0;
                tamVet=0;
                
            }
            
        }
        
        if (acumularTam != 0) {
            char URL2[100];
            char URLAux[100];
            
            strcpy(URL2, URLPastaDestino);
            contArq++;
            sprintf(URLAux, "/divisao%d.dat",contArq);
            strcat(URL2, URLAux);
            
            FILE* arq2 = fopen(URL2, "w");
            
            if (arq2 == NULL) {  // Isso nao pode acontecer
                printf("Arquivo divisao%d.dat não pode ser criado!",contArq);
                exit(0);
            } else {
                
                Quick(vet, 0, tamVet-1);
                
                int count;
                for (count=0; count<tamVet; count++) {
                    
                    escreverZerosRestantesNoArquivo(arq2, vet[count]);
                    
                    fprintf(arq2, "%d ",vet[count]);
                }
                fclose(arq2);
            }
        }
        
        fclose(arq);
        
        return contArq;
    }
    return 0;
}

//    Função que gera o arquivo de entrada no diretório URL, com um limite de valor qu pode ser gerado e o tamanho do arquivo a ser gerado. Retorna 0(zero) caso o arquivo não tenha sido gerado corretamente, e retorna 1(um) caso o arquivo tenha sido criado com sucesso.
int gerarArquivoDeEntrada(char URL[], int maiorValorQuePodeSerGerado, long double tamanhoDoArquivoEmKB){
    
    FILE *arq = fopen(URL, "w");         //  Carrega o arquivo de entrada
    srand((unsigned)time(NULL));
    
    if (arq == NULL) {
        
        fclose(arq);
        
        return 0;
    } else {
     	 
        long long int acumularTam = 0;         // Variável que gerencia o tamanho de armazenamento
        long long int tamanhoDoArquivoEmBy = tamanhoDoArquivoEmKB*1024;
        
        while (acumularTam + 11 < tamanhoDoArquivoEmBy) {
//            int X = rand() % maiorValorQuePodeSerGerado;       //  Gera um número aleatório
            uint64_t X;

			/* add code to seed random number generator */
			
			X = rand();
			X = (X << 32) | rand();
			
			// enforce limits of value between 100000000 and 999999999
			X = (X % (999999999 - 1)) + 1;
            
            int numCarac = numeroDeCaracteresDentroDeUmInteiro(X);
            
            int numZeros = NUMCARACT - numCarac;
            
            int cont;
            for(cont = 0;cont < numZeros; cont++){
                fprintf(arq, "0");
                acumularTam++;
            }
            
            if (X != 0) {
                fprintf(arq, "%d ",X);
                acumularTam += numCarac + 1; // +1 por causa do espaço entre os números gerados
            }
            
            numeroDeElementosDoArquivoDeEntrada++;
            
        }
        
        fclose(arq);
        
        return 1;
    }
    
}

// Retorna o número de caracteres de um inteiro passado por parâmetro. Serve para saber quantos zeros são necessários para preencher as casas na hora de escrever o valor no arquivo
int numeroDeCaracteresDentroDeUmInteiro(int inteiro){
    int cont = 0;
    while (inteiro > 0) {
        inteiro = inteiro / 10;
        cont++;
    }
    
    return cont;
}

// Escreve em arq os zeros que faltam para preencher as casas decimais à esquerda do valor.
void escreverZerosRestantesNoArquivo(FILE* arq, int valor){
    int numCarac = numeroDeCaracteresDentroDeUmInteiro(valor);
    int numZeros = NUMCARACT - numCarac;
    
    int i;
    for (i=0; i<numZeros; i++) {
        fprintf(arq, "0");
    }
}

// Verifica se URL do arquivo é válido
int verificarExistenciaDeArquivo(char URLArq[]){
    FILE *arq = fopen(URLArq, "r");
    
    if (arq == NULL) {
        fclose(arq);
        return 0;
    } else {
        fclose(arq);
        return 1;
    }
}

// Verifica se URL da pasta é válido
int verificarExistenciaDaPasta(char URLArq[]){
    char aux[100];
    strcpy(aux, URLArq);
    strcat(aux, "arquivoTeste.dat");
    
    FILE* arq = fopen(aux, "w");
    
    if (arq == NULL) {
        fclose(arq);
        return 0;
    } else {
        fclose(arq);
        remove(aux);
        return 1;
    }
}

// Pede do usuário todas as informações necessárias para a execução do programa
void pedirInformacoesDoUsuario(char URLPastaArquivoDeEntrada[], char URLPastaArquivoDeSaida[], double *memoriaMaximaEmMB, int *K){
    char op1;
    
    do{
        printf("\nDeseja gerar o arquivo de entrada?\n");
        printf("(Digite 'S' ou 's' para gerar o arquivo de entrada\nou digite 'N' ou 'n' caso ja tenha o arquivo de entrada)\n");
        
        scanf("%c",&op1);
    }while(op1 != 'S' && op1 != 's' && op1 != 'N' && op1 != 'n');
    
    if (op1 == 'S' || op1 == 's') {
        double tamanhoArquivoEmMB;
        printf("\nInforme o tamanho em MB(Megabytes) do arquivo que deseja gerar:\n");
        scanf("%lf",&tamanhoArquivoEmMB);
        
        int flag = 0;
        
        do{
            printf("\nInforme o diretorio da pasta que deseja gerar o arquivo de entrada:\n(Ex: \\users\\mariohenrique\\desktop)\n");            // MUDAR AQUI NO WINDOWS
            fflush(stdin);
            gets(URLPastaArquivoDeEntrada);
            
            char URLAux[200];
            strcpy(URLAux, URLPastaArquivoDeEntrada);
            strcat(URLAux, "/Arquivo de Entrada.dat");
            
            printf("Carregando..");
            
            flag = gerarArquivoDeEntrada(URLAux, 2147483648, tamanhoArquivoEmMB*1024);
            
            if (!flag) {
                printf("\nDiretorio invalido! Tente novamente.\n");
            }
            
        }while(!flag);
        
    } else {
        
        int flag = 0;
        
        do{
            
            printf("\nInforme o diretorio da pasta no qual o 'Arquivo de Entrada.dat' está contido:\n(Ex: \\users\\mariohenrique\\desktop)\n");            // MUDAR AQUI NO WINDOWS
            gets( URLPastaArquivoDeEntrada);
            
            char URLAux[200];
            strcpy(URLAux, URLPastaArquivoDeEntrada);
            strcat(URLAux, "/Arquivo de Entrada.dat");
            
            flag = verificarExistenciaDeArquivo(URLAux);
            
            if (!flag) {
                printf("\n'Arquivo de Entrada.dat' nao se encontra neste diretorio! Tente novamente.\n");
            }
            
        }while(!flag);
        
    }
    
    // Chegando aqui, o 'Arquivo de Entrada.dat' com certeza existe e está contido em 'URLPastaArquivoDeEntrada'
    
    char op2;
    
    do {
        printf("\nDeseja que o arquivo de saida seja gerado no mesmo diretorio do arquivo de entrada?\n('S' ou 's' para Sim ou 'N' ou 'n' para Nao)\n");
        fflush(stdin);
        scanf("%c",&op2);
    }while(op2 != 'S' && op2 != 's' && op2 != 'N' && op2 != 'n');
    
    if (op2 == 's' || op2 == 'S') {
        strcpy(URLPastaArquivoDeSaida, URLPastaArquivoDeEntrada);
    } else {
        
        int flag = 0;
        
        do{
            printf("\nInforme o diretorio da pasta a ser gerado o arquivo de saida:\n(Ex: \\users\\mariohenrique\\desktop)\n");            // MUDAR AQUI NO WINDOWS
            gets(URLPastaArquivoDeSaida);
            
            flag = verificarExistenciaDaPasta(URLPastaArquivoDeSaida);
            
            if (!flag) {
                printf("\nDiretorio invalido! Tente novamente.\n");
            }
            
        }while(!flag);
    }
    
    // Chegando aqui, o diretorio do arquivo de saida com certeza está correto
    
    printf("\nInforme a quantidade total de memoria em MB(megabytes) disponivel para a ordenação:\n");
    scanf("%lf",memoriaMaximaEmMB);
    
    do{
        printf("\nInforme o valor de K:\n");
        fflush(stdin);
        scanf("%d",K);
        
        if (*K < 2) {
            printf("\nO valor de K deve ser maior que 1. Tente novamente.\n");
        }
        
    }while(*K < 2);
    
    printf("\nCarregando..");
    
    // Chegando aqui, o usuário precisa informar mais nenhuma informação
}

